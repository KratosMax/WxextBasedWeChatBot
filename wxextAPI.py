import requests


class WXbot(object):
    def __init__(self, nickname="西瓜籽", pid=0, apiurl="http://127.0.0.1:8203/api?json"):
        print("微信机器人%s成功创建" % nickname)
        self.nickname = nickname
        self.url = apiurl
        self.pid = pid
        userinfo = self.GetUserInfo()
        # 微信名
        self.name = userinfo["data"]["nickName"]
        # 微信号
        self.alias = userinfo["data"]["alias"]
        # 微信id
        self.wxid = userinfo["data"]["wxid"]

    def postApi(self, data):
        response = requests.post(url=self.url, data=data)
        if response:
            return response.json()
        return 0

    # 抖动微信
    def ShowWX(self):
        data = {
            "method": "show",
            "pid": self.pid
        }
        return self.postApi(data)

    # 运行一个微信，若已经有了则连接
    def RunWX(self):
        data = {
            "method": "run",
            "pid": 0
        }
        return self.postApi(data)

    # Kill掉当前进程，以备不时之需
    def KillWX(self):
        data = {
            "method": "kill",
            "pid": self.pid
        }
        return self.postApi(data)

    # 获取用户登录信息
    def GetUserInfo(self):
        data = {
            "method": "getInfo",
            "pid": self.pid
        }
        return self.postApi(data)

    # 获取用户通讯录
    def GetAllList(self):
        data = {
            "method": "getUser",
            "pid": self.pid
        }
        return self.postApi(data)

    # 获取群聊列表
    def GetGroup(self):
        data = {
            "method": "getGroup",
            "pid": self.pid
        }
        return self.postApi(data)

    # 获取群聊成员列表
    def GetGroupUser(self, wxid):
        data = {
            "method": "getGroupUser",
            "wxid": wxid,
            "pid": self.pid
        }
        return self.postApi(data)

    # 同意添加好友
    def AgreeUser(self, v1, v2):
        data = {
            "method": "agreeUser",
            "v1": v1,
            "v2": v2,
            "pid": self.pid
        }
        return self.postApi(data)

    # 发送文本消息
    def SendText(self, wxid, msg, atid=""):
        data = {
            "method": "sendText",
            "wxid": wxid,
            "msg": msg,
            "atid": atid,
            "pid": self.pid
        }
        return self.postApi(data)

    # 发送群聊邀请 wxid指群聊id，msg指目标用户
    def SendGroupInvite(self, groupwxid, userwxid):
        data = {
            "method": "sendGroupInvite",
            "wxid": groupwxid,
            "msg": userwxid,
            "pid": self.pid
        }
        return self.postApi(data)

    # 发送文件 支持file和url，file：路径记得将\转为\\，url则是链接
    def SendFile(self, wxid, file, fileType="file"):
        data = {
            "method": "sendFile",
            "wxid": wxid,
            "file": file,
            "fileType": fileType,
            "pid": self.pid
        }
        return self.postApi(data)

    # 发送图片 支持file和url，file：路径记得将\转为\\，url则是链接
    def SendImage(self, wxid, file, fileType="file"):
        data = {
            "method": "sendFile",
            "wxid": wxid,
            "file": file,
            "imgType": fileType,
            "pid": self.pid
        }
        return self.postApi(data)

    # 转发小程序和文章
    def SendAppmsgForward(self, wxid, msgxml):
        data = {
                "method": "sendAppmsgForward",
                "wxid": wxid,
                "xml": msgxml,
                "pid": self.pid
            }
        return self.postApi(data)

    # 发送名片
    def SendUserCard(self, wxid, userwxid):
        data = {
            "method": "sendCard",
            "wxid": wxid,
            "userid": userwxid,
            "pid": self.pid
        }
        return self.postApi(data)

    # 发送动图
    def SendEmoji(self, wxid, msgxml):
        data = {
            "method": "sendEmojiForward",
            "wxid": wxid,
            "xml": msgxml,
            "pid": self.pid
        }
        return self.postApi(data)


if __name__ == '__main__':
    bot = WXbot()
    print(bot.ShowWX())
